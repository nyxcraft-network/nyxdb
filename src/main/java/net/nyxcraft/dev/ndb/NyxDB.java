package net.nyxcraft.dev.ndb;

import net.nyxcraft.dev.ndb.mongodb.ResourceManager;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class NyxDB {

    private static DBPlugin plugin;

    public static void setPlugin(DBPlugin p) {
        plugin = p;
    }

    public static DBPlugin getPlugin() {
        return plugin;
    }

    public static ResourceManager getResourceManager() {
        return plugin.getMongoController().getResourceManager();
    }

    public static Datastore getDatastore() {
        return plugin.getMongoController().getDatastore();
    }

    public static void registerDAO(BasicDAO dao, Class<?> entity) {
        plugin.getMongoController().register(dao, entity);
    }

}
