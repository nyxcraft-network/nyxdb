package net.nyxcraft.dev.ndb.config;

import net.nyxcraft.dev.ndb.config.components.Database;
import net.nyxcraft.dev.ndb.utils.JsonConfig;

public class Settings extends JsonConfig {

    private Database database = new Database();

    public Database getDatabase() {
        return database;
    }
}
