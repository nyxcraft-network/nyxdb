package net.nyxcraft.dev.ndb;

import net.nyxcraft.dev.ndb.config.Settings;
import net.nyxcraft.dev.ndb.mongodb.MongoController;

import java.util.logging.Logger;

public interface DBPlugin {

    public Settings getSettings();

    public Logger getLogger();

    public MongoController getMongoController();

}
