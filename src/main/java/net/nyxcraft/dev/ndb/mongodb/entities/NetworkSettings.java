package net.nyxcraft.dev.ndb.mongodb.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity(value = "network_settings", noClassnameStored = true)
public class NetworkSettings {

    @Id
    public ObjectId id;
    public List<String> filteredWords = new ArrayList<>();
    public boolean maintenanceMode = false;
    public Map<String, List<String>> announcements = new HashMap<>();

}
