package net.nyxcraft.dev.ndb.mongodb.dao;

import net.nyxcraft.dev.ndb.mongodb.entities.NetworkSettings;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class NetworkSettingsDAO extends BasicDAO<NetworkSettings, ObjectId> {

    private static NetworkSettingsDAO instance;

    public NetworkSettingsDAO(Datastore ds) {
        super(ds);
        instance = this;
    }

    public static NetworkSettingsDAO getInstance() {
        return instance;
    }
}
