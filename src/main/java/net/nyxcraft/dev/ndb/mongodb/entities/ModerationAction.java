package net.nyxcraft.dev.ndb.mongodb.entities;

public enum ModerationAction {

    BAN,
    KICK,
    MUTE;

}
