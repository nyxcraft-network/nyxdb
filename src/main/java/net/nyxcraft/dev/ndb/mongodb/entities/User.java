package net.nyxcraft.dev.ndb.mongodb.entities;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.*;

import java.util.ArrayList;
import java.util.List;

@Entity(value = "network_users", noClassnameStored = true)
public class User {

    /**
     * Network Data
     */
    @Id
    public ObjectId id;
    @Indexed(unique = true)
    public String uuid;
    @Indexed
    public String name;
    public List<String> nameHistory = new ArrayList<>();
    public String ip;
    public List<String> ipHistory = new ArrayList<>();
    public String rank = "DEFAULT";
    @Embedded
    public UserSettings userSettings = new UserSettings();
    @Embedded
    public ModerationHistory moderationHistory = new ModerationHistory();
    public long playTime = 0;
    public long lastOnline = 0;
    public String lastServer = "";
    public int gems = 0;
    public int shards = 0;

    public User(String uuid) {
        this.uuid = uuid;
    }

    public User() {}

    @PostLoad void preLoad() {
        if (moderationHistory == null) {
            moderationHistory = new ModerationHistory();
        }

        if (lastServer == null) {
            lastServer = "";
        }
    }
}
