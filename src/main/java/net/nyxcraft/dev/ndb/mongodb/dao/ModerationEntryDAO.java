package net.nyxcraft.dev.ndb.mongodb.dao;

import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class ModerationEntryDAO extends BasicDAO<ModerationEntry, ObjectId> {

    private static ModerationEntryDAO instance;

    public ModerationEntryDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static ModerationEntryDAO getInstance() {
        return instance;
    }

}
