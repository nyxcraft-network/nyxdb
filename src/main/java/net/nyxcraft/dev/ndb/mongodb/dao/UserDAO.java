package net.nyxcraft.dev.ndb.mongodb.dao;

import net.nyxcraft.dev.ndb.mongodb.entities.User;
import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

public class UserDAO extends BasicDAO<User, ObjectId> {

    private static UserDAO instance;

    public UserDAO(Datastore datastore) {
        super(datastore);
        instance = this;
    }

    public static UserDAO getInstance() {
        return instance;
    }
}
