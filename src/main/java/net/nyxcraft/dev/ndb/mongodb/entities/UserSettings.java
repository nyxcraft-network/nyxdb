package net.nyxcraft.dev.ndb.mongodb.entities;

public class UserSettings {

    public boolean playersVisible = true;
    public boolean chatEnabled = true;
    public boolean acceptPrivateMessages = true;
    public boolean filterFoulLanguage = true;

}
