package net.nyxcraft.dev.ndb.mongodb;

import net.nyxcraft.dev.ndb.DBPlugin;
import net.nyxcraft.dev.ndb.mongodb.dao.ModerationEntryDAO;
import net.nyxcraft.dev.ndb.mongodb.dao.NetworkSettingsDAO;
import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.ModerationEntry;
import net.nyxcraft.dev.ndb.mongodb.entities.NetworkSettings;
import net.nyxcraft.dev.ndb.mongodb.entities.User;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.dao.BasicDAO;

import java.util.HashMap;
import java.util.Map;

@SuppressWarnings("rawtypes")
public class MongoController {

    private ResourceManager resourceManager;
    private Datastore datastore;
    private Map<Class, BasicDAO> daoMap = new HashMap<>();

    public MongoController(DBPlugin plugin) {
        resourceManager = new ResourceManager(plugin);
        datastore = resourceManager.getDatastore("net.nyxcraft.dev.ndb.mongodb.entities");

        init();
    }

    public void register(BasicDAO dao, Class<?> entity) {
        datastore.ensureCaps();
        datastore.ensureIndexes(entity);

        daoMap.put(entity, dao);
    }

    public void init() {
        register(new UserDAO(datastore), User.class);
        register(new ModerationEntryDAO(datastore), ModerationEntry.class);
        register(new NetworkSettingsDAO(datastore), NetworkSettings.class);
    }

    public ResourceManager getResourceManager() {
        return resourceManager;
    }

    public Datastore getDatastore() {
        return datastore;
    }
}
