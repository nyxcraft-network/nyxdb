package net.nyxcraft.dev.ndb.mongodb;

import java.util.UUID;

import net.nyxcraft.dev.ndb.mongodb.dao.ModerationEntryDAO;
import net.nyxcraft.dev.ndb.mongodb.dao.NetworkSettingsDAO;
import net.nyxcraft.dev.ndb.mongodb.dao.UserDAO;
import net.nyxcraft.dev.ndb.mongodb.entities.*;
import org.mongodb.morphia.query.Query;
import org.mongodb.morphia.query.QueryResults;
import org.mongodb.morphia.query.UpdateOperations;

public class DataAPI {

    /**
     * Initializes a network user instance for an online player.
     *
     * @param name the name of the user
     * @param uuid the uuid of the user
     * @param ip the ip of the user
     * @return Networkuser instance
     */
    public static User initUser(UUID uuid, String name, String ip) {
        User user = retrieveUser(uuid);

        if (user == null) {
            user = new User(uuid.toString());
        }

        updateUser(user, name, ip);

        return user;
    }

    /**
     * Updates the core data of a user.
     *
     * @param user
     * @param name
     * @param ip
     */
    public static void updateUser(User user, String name, String ip) {
        boolean updated = false;
        if (user.name == null || user.name.equalsIgnoreCase(name) == false) {
            if (user.name != null) {
                user.nameHistory.add(user.name);
            }

            user.name = name;
            updated = true;
        }

        if (user.ip == null || user.ip.equalsIgnoreCase(ip) == false) {
            if (user.ip != null) {
                user.ipHistory.add(user.ip);
            }

            user.ip = ip;
            updated = true;
        }

        if (updated) {
            UserDAO.getInstance().save(user);
        }
    }

    /**
     * Retrieves a profile matching the specified uuid.
     *
     * @param uuid
     * @return
     */
    public static User retrieveUser(UUID uuid) {
        return UserDAO.getInstance().findOne("uuid", uuid.toString());
    }

    /**
     * Retrieves a profile matching the specified name.
     *
     * @param name
     * @return
     */
    public static User retrieveUserByName(String name) {
        for (User user : UserDAO.getInstance().createQuery().field("name").containsIgnoreCase(name).fetch()) {
            if (user.name.equalsIgnoreCase(name)) {
                return user;
            }
        }

        return null;
    }

    public static void updateUserSettings(UUID uuid, UserSettings settings) {
        UpdateOperations<User> operations = UserDAO.getInstance().createUpdateOperations();
        operations.set("userSettings", settings);

        Query<User> query = UserDAO.getInstance().createQuery().field("uuid").equal(uuid.toString());

        UserDAO.getInstance().update(query, operations);
    }

    public static void addModerationEntry(ModerationEntry entry) {
        ModerationEntryDAO.getInstance().save(entry);

        UpdateOperations<User> ops = UserDAO.getInstance().createUpdateOperations();
        ops.add("moderationHistory.entries", entry);

        if (entry.action == ModerationAction.BAN) {
            ops.set("moderationHistory.activeBan", entry);
        } else if (entry.action  == ModerationAction.MUTE) {
            ops.set("moderationHistory.activeMute", entry);
        }

        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("uuid").equal(entry.uuid), ops);
    }

    public static void setTimeStats(UUID uuid, long playTime, long lastOnline) {
        UpdateOperations<User> operations = UserDAO.getInstance().createUpdateOperations();
        operations.inc("playTime", playTime);
        operations.set("lastOnline", lastOnline);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("uuid").equal(uuid.toString()), operations);
    }

    public static void setLastServer(UUID uuid, String name) {
        UpdateOperations<User> operations = UserDAO.getInstance().createUpdateOperations();
        operations.set("lastServer", name);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("uuid").equal(uuid.toString()), operations);
    }

    public static void addFilteredWord(String word) {
        UpdateOperations<NetworkSettings> operations = NetworkSettingsDAO.getInstance().createUpdateOperations();
        operations.add("filteredWords", word);
        NetworkSettingsDAO.getInstance().update(NetworkSettingsDAO.getInstance().createQuery(), operations);
    }

    public static NetworkSettings getNetworkSettings() {
        QueryResults<NetworkSettings> results = NetworkSettingsDAO.getInstance().find();
        NetworkSettings settings = (results != null ? results.get() : null);
        if (settings == null) {
            settings = new NetworkSettings();
            NetworkSettingsDAO.getInstance().save(settings);
        }
        return settings;
    }

    public static void updateShards(UUID uuid, int shards) {
        UpdateOperations<User> operations = UserDAO.getInstance().createUpdateOperations();
        operations.inc("shards", shards);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("uuid").equal(uuid.toString()), operations);
    }

    public static void updateGems(UUID uuid, int gems) {
        UpdateOperations<User> operations = UserDAO.getInstance().createUpdateOperations();
        operations.inc("gems", gems);
        UserDAO.getInstance().update(UserDAO.getInstance().createQuery().field("uuid").equal(uuid.toString()), operations);
    }
}
