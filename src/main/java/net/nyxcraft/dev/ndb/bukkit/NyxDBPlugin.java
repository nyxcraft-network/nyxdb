package net.nyxcraft.dev.ndb.bukkit;

import net.nyxcraft.dev.ndb.DBPlugin;
import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.config.Settings;
import net.nyxcraft.dev.ndb.mongodb.MongoController;
import net.nyxcraft.dev.ndb.utils.JsonConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class NyxDBPlugin extends JavaPlugin implements DBPlugin {

    private Settings settings = null;
    private MongoController mongoController;

    public void onEnable() {
        NyxDB.setPlugin(this);

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        mongoController = new MongoController(this);
    }

    public Settings getSettings() {
        return settings;
    }

    public MongoController getMongoController() {
        return mongoController;
    }
}
