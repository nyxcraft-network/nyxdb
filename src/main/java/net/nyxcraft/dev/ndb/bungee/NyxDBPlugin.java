package net.nyxcraft.dev.ndb.bungee;

import net.md_5.bungee.api.plugin.Plugin;
import net.nyxcraft.dev.ndb.DBPlugin;
import net.nyxcraft.dev.ndb.NyxDB;
import net.nyxcraft.dev.ndb.config.Settings;
import net.nyxcraft.dev.ndb.mongodb.MongoController;
import net.nyxcraft.dev.ndb.utils.JsonConfig;

import java.io.File;

public class NyxDBPlugin extends Plugin implements DBPlugin {

    private Settings settings = null;
    private MongoController mongoController;

    public void onEnable() {
        NyxDB.setPlugin(this);

        if (!getDataFolder().exists()) {
            getLogger().info("Config folder not found! Creating...");
            getDataFolder().mkdir();
        }

        settings = JsonConfig.load(new File(getDataFolder(), "settings.json"), Settings.class);
        mongoController = new MongoController(this);
    }

    public Settings getSettings() {
        return settings;
    }

    public MongoController getMongoController() {
        return mongoController;
    }
}
